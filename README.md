# perplexity-api v0.2.3 (no longer maintained)

An unofficial Perplexity.ai Python API

## Description

Perplexity AI is an AI-powered search engine that uses machine learning and natural language processing to provide precise and accurate answers to user questions. This unofficial API provides a way to interact with it from Python.


## Installation

Download the [Chrome WebDriver](https://chromedriver.chromium.org/downloads)

```bash
python -m pip install --user git+https://gitlab.com/robowaifudev/perplexity-api
```

## Example
```python
from perplexity_api import PerplexityAPI, TimeoutException
ppl = PerplexityAPI()

queries = [
    "hello world in python",
    "and in c++",
]

for i, query in enumerate(queries):
    if i > 0:
        print("***")
    print(query)
    print("***")
    try:
        print(ppl.query(query, follow_up=True))
    except TimeoutException:
        print("Query timed out:", query)
```

**NEW** Writing focus
```python
ppl.query("Write a cute short story about Touhou fumos.", focus="writing")
```

## Roadmap

* Add error handling
* Return references

## License
MIT

## Project status
Actively working this but updates will be slow.
